package com.wakeel.currencies.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.wakeel.currencies.data.CurrencyResponse
import com.wakeel.currencies.data.network.ApiClient

class CurrencyViewModel : ViewModel() {
    private val update: Any = Object()

    private val newCurrenciesObs: Observer<CurrencyResponse>

    init {
        newCurrenciesObs = Observer {
            if (it != null) updateLatestCurrencies(it)
        }
        ApiClient.getLatestRates().observeForever(newCurrenciesObs)
    }

    private var baseCurrency: String = "EUR"
    private var baseValue: Float = 1F
    private val currencyRates: MutableLiveData<List<CurrencyModel>> = MutableLiveData()

    private fun updateLatestCurrencies(currency: CurrencyResponse) {
        val newCurrencyModels: MutableList<CurrencyModel> = mutableListOf()
        synchronized(update) {
            newCurrencyModels.add(CurrencyModel(currency.baseCurrency!!, 1.0F, baseValue))
            currency.rates?.forEach { (currency, rate) ->
                newCurrencyModels.add(CurrencyModel(currency, rate, rate * baseValue))
            }
            if (currencyRates != null)
                currencyRates.value = newCurrencyModels
        }
    }

    fun getCurrencyRates(): LiveData<List<CurrencyModel>> = currencyRates

    fun refreshRates() {
        ApiClient.refreshCurrencies(baseCurrency)
    }

    fun setNewBase(newBaseCurrency: String, newBaseValue: Float) {
        if (baseCurrency == newBaseCurrency)
            return
        baseCurrency = newBaseCurrency
        baseValue = newBaseValue
        refreshRates()
    }

    fun setNewBaseValue(value: Float) {
        if (baseValue.equals(value))
            return

        synchronized(update) {
            baseValue = value
            val newCurrencyModels: MutableList<CurrencyModel> = mutableListOf()
            currencyRates.value?.forEach {
                newCurrencyModels.add(
                    CurrencyModel(
                        it.currency,
                        it.rate,
                        it.rate * baseValue
                    )
                )
            }
            currencyRates.value = newCurrencyModels
        }
    }

    override fun onCleared() {
        ApiClient.getLatestRates().removeObserver(newCurrenciesObs)
    }

    data class CurrencyModel(
        val currency: String,
        val rate: Float,
        var value: Float
    )
}