package com.wakeel.currencies.view

import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.wakeel.currencies.R

class CurrenciesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val flagImg: ImageView = itemView.findViewById(R.id.flag_img)
    val currencyTitleTxt: TextView = itemView.findViewById(R.id.currency_title_txt)
    val currencyNameTxt: TextView = itemView.findViewById(R.id.currency_name_txt)
    val currencyInputTxt: EditText = itemView.findViewById(R.id.currency_input_txt)
    val currencyContainer: ConstraintLayout = itemView.findViewById(R.id.currency_container)
}
