package com.wakeel.currencies.view

interface OnChangeCurrencyCallBack {
    fun onCurrencyChanged(base: String, value: Float)
    fun onValueChanged(value: Float)
    fun scrollToTop()
}