package com.wakeel.currencies.view

import androidx.recyclerview.widget.DiffUtil
import com.wakeel.currencies.viewmodel.CurrencyViewModel

class CalculateCurrency(
    private val oldList: List<CurrencyViewModel.CurrencyModel>,
    private val newList: List<CurrencyViewModel.CurrencyModel>
) : DiffUtil.Callback() {

    companion object {
        const val VALUE_CHG = "VALUE_CHG"
    }

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].currency == newList[newItemPosition].currency
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val oldRate = oldList[oldItemPosition]
        val newRate = newList[newItemPosition]

        val payloadSet = mutableSetOf<String>()

        if (oldRate.value != newRate.value)
            payloadSet.add(VALUE_CHG)

        return payloadSet
    }
}