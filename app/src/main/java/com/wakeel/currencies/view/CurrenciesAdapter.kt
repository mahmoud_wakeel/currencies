package com.wakeel.currencies.view

import android.annotation.SuppressLint
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.wakeel.currencies.R
import com.wakeel.currencies.viewmodel.CurrencyViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*


class CurrenciesAdapter(val callback: OnChangeCurrencyCallBack) :
    RecyclerView.Adapter<CurrenciesViewHolder>() {
    private var currenciesList: List<CurrencyViewModel.CurrencyModel>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrenciesViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.currency_item, parent, false)
        val rateHolder = CurrenciesViewHolder(view)
        rateHolder.currencyContainer.setOnClickListener {
            val pos: Int = rateHolder.adapterPosition
            if (pos != RecyclerView.NO_POSITION && pos > 0) {
                callback.onCurrencyChanged(
                    currenciesList!![pos].currency,
                    currenciesList!![pos].value
                )
            }
        }
        return rateHolder
    }

    override fun getItemCount(): Int {
        return if (currenciesList == null) 0 else currenciesList!!.size
    }

    private val currencyWatcher: TextWatcher = object : TextWatcher {
        override fun afterTextChanged(newValue: Editable?) {
            val strValue: String = newValue.toString().trim()
            val value: Float
            value = try {
                strValue.toFloat()
            } catch (e: Exception) {
                0F
            }
            currenciesList!![0].value = value
            callback.onValueChanged(value)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }
    }

    override fun onBindViewHolder(holder: CurrenciesViewHolder, position: Int) {
        val currencyModel = currenciesList?.get(position)
        holder.currencyInputTxt.isEnabled = position == 0
        holder.currencyInputTxt.removeTextChangedListener(currencyWatcher)
        if (position == 0)
            holder.currencyInputTxt.addTextChangedListener(currencyWatcher)
        val newValue: String = "%.2f".format(currencyModel!!.value)
        if (holder.currencyInputTxt.text.toString() != newValue) {
            holder.currencyInputTxt.setText(newValue)
        }
        val updatedCurrency = currencyModel.currency
        holder.currencyTitleTxt.text = updatedCurrency
        holder.currencyNameTxt.text = getCurrencyDesc(holder, updatedCurrency)

        val flagDrawable = ResourcesCompat.getDrawable(
            holder.flagImg.resources,
            getCurrencyFlag(holder, updatedCurrency),
            null
        )
        holder.flagImg.setImageDrawable(flagDrawable)
    }

    private fun getCurrencyDesc(holder: CurrenciesViewHolder, currencyName: String): String {
        val currencyDescName: String
        val resources = holder.currencyNameTxt.context.resources
        val packageName = holder.currencyNameTxt.context.packageName
        currencyDescName = try {
            resources.getString(resources.getIdentifier(currencyName, "string", packageName))
        } catch (e: Exception) {
            "N/A"
        }
        return currencyDescName
    }

    @SuppressLint("DefaultLocale")
    private fun getCurrencyFlag(holder: CurrenciesViewHolder, currencyName: String): Int {
        var currencyFlag: Int
        val resources = holder.flagImg.context.resources
        val packageName = holder.flagImg.context.packageName
        val drawableName = currencyName.toLowerCase()
        currencyFlag = try {
            resources.getIdentifier(drawableName, "drawable", packageName)
        } catch (e: Exception) {
            0
        }
        if (currencyFlag == 0)
            currencyFlag = R.drawable.eur
        return currencyFlag
    }

    private val pendingList: Deque<List<CurrencyViewModel.CurrencyModel>> = LinkedList()
    suspend fun updateList(newRatesList: List<CurrencyViewModel.CurrencyModel>) {
        if (currenciesList == null) {
            currenciesList = newRatesList
            notifyItemRangeInserted(0, newRatesList.size)
            return
        }

        pendingList.push(newRatesList)

        if (pendingList.size > 1)
            return

        calculateDiff(newRatesList)
    }

    private suspend fun calculateDiff(latest: List<CurrencyViewModel.CurrencyModel>) {
        val currenciesAdapter = this
        withContext(Dispatchers.Default) {
            val diffResult = DiffUtil.calculateDiff(CalculateCurrency(currenciesList!!, latest))
            val newRootRate: Boolean = (currenciesList!![0].currency != latest[0].currency)
            currenciesList = latest
            pendingList.remove(latest)
            withContext(Dispatchers.Main) {
                diffResult.dispatchUpdatesTo(currenciesAdapter)
                if (newRootRate)
                    callback.scrollToTop()
            }
            if (pendingList.size > 0) {
                calculateDiff(pendingList.pop())
                pendingList.clear()
            }
        }
    }
}