package com.wakeel.currencies.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.wakeel.currencies.R
import com.wakeel.currencies.viewmodel.CurrencyViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity(), OnChangeCurrencyCallBack {
    private var currenciesAdapter: CurrenciesAdapter? = null
    private var currencyViewModel: CurrencyViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        currencyViewModel =
            ViewModelProvider(this).get(CurrencyViewModel::class.java)
        currencyViewModel?.refreshRates()
        currenciesAdapter = CurrenciesAdapter((this))
        val layoutManager = LinearLayoutManager(this)
        currency_list.layoutManager = layoutManager
        currency_list.addItemDecoration(
            DividerItemDecoration(
                currency_list.context,
                layoutManager.orientation
            )
        )
        currency_list.adapter = currenciesAdapter
        currencyViewModel?.getCurrencyRates()?.observe(this,
            Observer { currencyRates ->
                if (currencyRates != null) {
                    GlobalScope.launch(Dispatchers.Main) {
                        currenciesAdapter?.updateList(currencyRates)
                    }
                }
            })
    }

    override fun onStart() {
        super.onStart()
        refreshCurrencies()
    }

    private fun refreshCurrencies() {
            val scheduleTaskExecutor: ScheduledExecutorService = Executors.newScheduledThreadPool(5)
            scheduleTaskExecutor.scheduleAtFixedRate(
                {
                    currencyViewModel?.refreshRates()
                },
                0,
                1,
                TimeUnit.SECONDS
            )
    }

    override fun onCurrencyChanged(base: String, value: Float) {
        currencyViewModel?.setNewBase(base, value)
    }

    override fun onValueChanged(value: Float) {
        currencyViewModel?.setNewBaseValue(value)
    }

    override fun scrollToTop() {
        currency_list.scrollToPosition(0)
    }
}
