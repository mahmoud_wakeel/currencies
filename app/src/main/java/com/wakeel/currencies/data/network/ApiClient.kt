package com.wakeel.currencies.data.network

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.wakeel.currencies.data.CurrencyResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {
    private const val baseURL = "https://hiring.revolut.codes/api/android/"
    private val currenciesWebServices: CurrenciesWebServices

    init {
        val client = Retrofit.Builder()
            .baseUrl(baseURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        currenciesWebServices = client.create(CurrenciesWebServices::class.java)
    }

    fun refreshCurrencies(baseCurrency: String) {
        currenciesWebServices.fetchCurrencies(baseCurrency)
            .enqueue(object : Callback<CurrencyResponse> {
                override fun onResponse(
                    call: Call<CurrencyResponse>,
                    response: Response<CurrencyResponse>
                ) {
                    if (response.isSuccessful)
                        updatedRates.value = response.body()
                }

                override fun onFailure(call: Call<CurrencyResponse>, t: Throwable) {
                    Log.e("ERROR", "ERROR")
                }
            })
    }

    private val updatedRates: MutableLiveData<CurrencyResponse> = MutableLiveData()

    fun getLatestRates(): LiveData<CurrencyResponse> = updatedRates
}