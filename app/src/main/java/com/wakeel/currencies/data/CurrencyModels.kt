package com.wakeel.currencies.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CurrencyResponse(
    @SerializedName("baseCurrency")
    @Expose
    val baseCurrency: String? = "",
    @SerializedName("rates")
    @Expose
    val rates: HashMap<String, Float>? = null
)