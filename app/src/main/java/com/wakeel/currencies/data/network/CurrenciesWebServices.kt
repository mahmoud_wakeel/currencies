package com.wakeel.currencies.data.network

import com.wakeel.currencies.data.CurrencyResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrenciesWebServices {
    @GET("latest")
    fun fetchCurrencies(@Query("base") base: String): Call<CurrencyResponse>
}